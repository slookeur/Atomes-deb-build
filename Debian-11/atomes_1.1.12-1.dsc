-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: atomes
Binary: atomes, atomes-data
Architecture: any all
Version: 1.1.12-1
Maintainer: Sébastien Le Roux <sebastien.leroux@ipcms.unistra.fr>
Homepage: https://atomes.ipcms.fr/
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), automake, autoconf, pkg-config, gfortran, libgfortran5, libgtk-3-dev, libxml2-dev, libpango1.0-dev, libglu1-mesa-dev, libepoxy-dev, libavutil-dev, libavcodec-dev, libavformat-dev, libswscale-dev, desktop-file-utils, appstream-util
Package-List:
 atomes deb science optional arch=any
 atomes-data deb science optional arch=all
Checksums-Sha1:
 46a92c7085b03dae8856039dfc2bafb074ae2492 2156392 atomes_1.1.12.orig.tar.xz
 1a2fa599d3aca56eb12a9bbb466cd14ebc9ac699 13584 atomes_1.1.12-1.debian.tar.xz
Checksums-Sha256:
 f45f51df7674d2062fbd5efffd550319df7b3c311730846e5c85c562765ff138 2156392 atomes_1.1.12.orig.tar.xz
 d787c32abd33b8b7974b20fce7e47be95b5b60196f43c10ca8e8bf302e9ff6ac 13584 atomes_1.1.12-1.debian.tar.xz
Files:
 cd5a967b9308ae41654ab9231b4faace 2156392 atomes_1.1.12.orig.tar.xz
 20c11549ed09b865c616157df2e14970 13584 atomes_1.1.12-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iI4EARYIADYWIQQkkm03PUIT9ReMNBuz4KPuLR7ZwwUCZLpVTBgcYXRvbWVzQGlw
Y21zLnVuaXN0cmEuZnIACgkQs+Cj7i0e2cO23gD/eyc0ZUY0qjZmWCdDIySBVVJb
QrI0vQxayTF/7VvviEUBAPPJRvsBBX6OGv51cUmT/i57vQ28G7g870BlAidT1bwJ
=GQNq
-----END PGP SIGNATURE-----
