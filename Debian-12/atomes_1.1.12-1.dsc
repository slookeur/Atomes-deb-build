-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: atomes
Binary: atomes, atomes-data
Architecture: any all
Version: 1.1.12-1
Maintainer: Sébastien Le Roux <sebastien.leroux@ipcms.unistra.fr>
Homepage: https://atomes.ipcms.fr/
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), automake, autoconf, pkg-config, gfortran, libgfortran5, libgtk-3-dev, libxml2-dev, libpango1.0-dev, libglu1-mesa-dev, libepoxy-dev, libavutil-dev, libavcodec-dev, libavformat-dev, libswscale-dev, desktop-file-utils, appstream-util
Package-List:
 atomes deb science optional arch=any
 atomes-data deb science optional arch=all
Checksums-Sha1:
 8821d44b3a085d09110f9ec79f178962b7f08e93 2141776 atomes_1.1.12.orig.tar.xz
 bb4fa6afd94261815df59ef2d29856f9755174fb 13592 atomes_1.1.12-1.debian.tar.xz
Checksums-Sha256:
 200e80c7c51e628004128b08a70b0a42f17e8fdbb2c93e03f6fe04a99b6ca99b 2141776 atomes_1.1.12.orig.tar.xz
 06d56abd7c542b4e387ade7b27f048de738f232bfa32fdd2cd87318347bb8ce7 13592 atomes_1.1.12-1.debian.tar.xz
Files:
 79512ddaadddd8161f26248ddad484c8 2141776 atomes_1.1.12.orig.tar.xz
 79791bd442d98e98a501ff1f9227354d 13592 atomes_1.1.12-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iI4EARYKADYWIQQkkm03PUIT9ReMNBuz4KPuLR7ZwwUCZLpWkRgcYXRvbWVzQGlw
Y21zLnVuaXN0cmEuZnIACgkQs+Cj7i0e2cPYAgEAxgiv3odeBSDS939qXHGwx7cj
0Of9zcJNOuCwynxC8ioBAIpyGvK9uW4RPNgKxo/5bOHv0qVO81EKX5/esku1RU0M
=X8/Z
-----END PGP SIGNATURE-----
